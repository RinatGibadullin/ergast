import {AxiosResponse} from "axios";
import instance from "../../../core/API";
import {DriverType} from "../interfaces/driverType";

type GetDriversResponseType = {
    MRData: {
        xmlns: string,
        series: string,
        limit: string,
        offset: string,
        total: string,
        DriverTable: {
            circuitId: string,
            constructorId: string,
            Drivers: Array<DriverType>
        }
    }
}

export const driversAPI = {
    getDrivers(offset: number, limit: number) {
        return instance.get(`drivers.json?limit=${limit}&offset=${offset}`)
            .then((response: AxiosResponse<GetDriversResponseType>) => response.data.MRData.DriverTable.Drivers)
    },
    getDriver(id: string) {
        return instance.get(`drivers/${id}.json`)
            .then((response: AxiosResponse<GetDriversResponseType>) => response.data.MRData.DriverTable.Drivers[0])
    }

}
