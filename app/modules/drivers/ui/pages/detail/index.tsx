import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {RootStateType} from "../../../../store/reducer";
import {connect, ConnectedProps} from "react-redux";
import {requestDriverDetail} from "../../../store/driver-detail/reducer";
import {driverDetailSelector} from "../../../store/driver-detail/selectors";
import {RootStackParamList} from "../../../../../../App";
import {StackScreenProps} from '@react-navigation/stack';
import DriverDetailInfo from "../../components/driver-page-info";

const mapState = (state: RootStateType) => ({
    driver: driverDetailSelector(state)
});

const mapDispatch = {
    requestDriverDetail
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>

type DriverDetailOwnProps = StackScreenProps<RootStackParamList, 'DRIVER'>;
type DriverDetailProps = PropsFromRedux & DriverDetailOwnProps

const DriverDetail: React.FC<DriverDetailProps> = ({route, driver, requestDriverDetail, ...props}) => {

    useEffect(() => {
        requestDriverDetail(route.params.id)
    }, [])

    return <View>
        <DriverDetailInfo driver={driver}/>
    </View>;
}

export default connector(DriverDetail)
