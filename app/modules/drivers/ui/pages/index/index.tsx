import React, {useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {RootStateType} from "../../../../store/reducer";
import {driversSelector, pagePaginationSelector} from "../../../store/drivers-list/selectors";
import {requestDrivers} from "../../../store/drivers-list/reducer";
import {connect, ConnectedProps} from "react-redux";
import {SET_DRIVERS_PAGINATION} from "../../../store/drivers-list/actions";
import {DriversList} from "../../components/drivers-list";

const mapState = (state: RootStateType) => ({
    drivers: driversSelector(state),
    pagination: pagePaginationSelector(state)
});

const mapDispatch = {
    requestDrivers,
    setPagePagination: () => ({type: SET_DRIVERS_PAGINATION}),
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>

const Drivers = (props: PropsFromRedux) => {

    useEffect(() => {
        props.requestDrivers(props.pagination.offset, props.pagination.limit)
    }, [props.pagination.offset])

    return <View style={styles.container}>
        <Text style={styles.title}>DRIVERS</Text>
        <DriversList drivers={props.drivers}/>
        <TouchableOpacity
            style={styles.button}
            onPress={props.setPagePagination}
        >
            <Text>Show more</Text>
        </TouchableOpacity>
    </View>;
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    title: {
        fontSize: 50,
        justifyContent: "center"
    },
    button: {
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10
    }
});

export default connector(Drivers)
