import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {DriverType} from "../../../domain/interfaces/driverType";

type DriverDetailInfoProps = {
    driver: DriverType
}
type Props = DriverDetailInfoProps

const DriverDetailInfo: React.FC<Props> = ({driver}) => {

    return <View style={styles.container}>
        <Text style={styles.title}>{driver.givenName} {driver.familyName}</Text>
        <Text>Date of birth: {driver.dateOfBirth}</Text>
        <Text>Nationality: {driver.nationality}</Text>
        <Text>Permanent number: {driver.permanentNumber}</Text>
        <Text>Wikipedia: {driver.url}</Text>
    </View>;
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    title: {
        fontSize: 50,
        justifyContent: "center"
    },
});

export default DriverDetailInfo
