import {StyleSheet, Text, TouchableHighlight, TouchableOpacity, View} from "react-native";
import React from "react";
import {DriverType} from "../../../domain/interfaces/driverType";
import {ItemCard} from "../../../../ud-ui/drvier-item-card/styles";
import {RootStackParamList} from "../../../../../../App";
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';


type OwnProps = {
    driver: DriverType,
};

type Props = OwnProps

export const DriversItem: React.FC<Props> = ({driver}) => {
    const navigation = useNavigation<StackNavigationProp<RootStackParamList, 'DRIVER'>>();
    return (
        <TouchableHighlight onPress={() => navigation.navigate('DRIVER', {id: driver.driverId})}>
            <ItemCard>
                <View style={styles.row}>
                    <Text style={{margin: 5}}>
                        {driver.givenName}
                    </Text>
                    <Text style={{margin: 5}}>
                        {driver.familyName}
                    </Text>
                    <Text style={{margin: 5}}>
                        {driver.permanentNumber}
                    </Text>
                </View>
                <View style={styles.row}>
                    <Text style={{margin: 5}}>
                        {driver.dateOfBirth}
                    </Text>
                    <Text style={{margin: 5}}>
                        {driver.code}
                    </Text>
                    <Text style={{margin: 5, backgroundColor: "red", borderRadius: 10, padding: 5}}>
                        {driver.nationality}
                    </Text>
                </View>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => navigation.navigate('DRIVER_RACES', {driver_id: driver.driverId})}
                >
                    <Text>Show races</Text>
                </TouchableOpacity>
            </ItemCard>
        </TouchableHighlight>
    );
}

const styles = StyleSheet.create({
    row: {
        flex: 1,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 10
    },
    button: {
        alignItems: "center",
        backgroundColor: "#979ddd",
        padding: 10
    }
});
