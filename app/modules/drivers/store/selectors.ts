import {RootStateType} from "../../store/reducer";


export const driversState = (state: RootStateType) => state.driversRootReducer.driversReducer

export const driverDetailState = (state: RootStateType) => state.driversRootReducer.driverDetailReducer
