import {createSelector} from "reselect";
import {RootStateType} from "../../../store/reducer";
import {driversState} from "../selectors";

const drivers = (state: RootStateType) => driversState(state).drivers
const pagePagination = (state: RootStateType) => driversState(state).pagination

export const driversSelector = createSelector(drivers, (drivers) => {
    return drivers
})

export const pagePaginationSelector = createSelector(pagePagination, (pagination) => {
    return pagination
})
