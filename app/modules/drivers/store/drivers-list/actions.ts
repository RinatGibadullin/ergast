import {DriverType} from "../../domain/interfaces/driverType";

export const SET_DRIVERS = 'SET_DRIVERS';
export const SET_DRIVERS_PAGINATION = 'SET_DRIVERS_PAGINATION';


export type DriversPagePagination = {
    offset: number,
    limit: number
}

export type setDriversActionType = {
    type: typeof SET_DRIVERS
    drivers: Array<DriverType>
}

export type setDriversPagePaginationActionType = {
    type: typeof SET_DRIVERS_PAGINATION
    pagination: DriversPagePagination
}

export const setDrivers = (drivers: Array<DriverType>): setDriversActionType => ({type: SET_DRIVERS, drivers});

export const setDriversPagination = (pagination: DriversPagePagination): setDriversPagePaginationActionType => (
    {type: SET_DRIVERS_PAGINATION, pagination}
);

export type DriversListActionsType = setDriversActionType | setDriversPagePaginationActionType
