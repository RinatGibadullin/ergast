import {Dispatch} from "redux";
import {RootStateType} from "../../../store/reducer";
import {DriverType} from "../../domain/interfaces/driverType";
import {
    DriversListActionsType,
    DriversPagePagination,
    SET_DRIVERS,
    SET_DRIVERS_PAGINATION,
    setDrivers
} from "./actions";
import {driversAPI} from "../../domain/API/driversAPI";

export const driversListInitialState = {
    drivers: [] as DriverType[],
    pagination: {
        offset: 0, limit: 10
    } as DriversPagePagination
}

type DriversListInitialStateType = typeof driversListInitialState

export const driversReducer = (
    state = driversListInitialState, action: DriversListActionsType
): DriversListInitialStateType => {
    switch (action.type) {
        case SET_DRIVERS:
            return {
                ...state,
                drivers: [...state.drivers, ...action.drivers]
            }
        case SET_DRIVERS_PAGINATION:
            return {
                ...state,
                pagination: {
                    ...state.pagination,
                    offset: (state.pagination.offset + state.pagination.limit)
                }
            }
        default:
            return state
    }
};

export const requestDrivers = (offset: number, limit: number) => {
    return async (dispatch: Dispatch<DriversListActionsType>, getState: RootStateType) => {
        let data = await driversAPI.getDrivers(offset, limit)
        dispatch(setDrivers(data))
    }
}
