import {createSelector} from "reselect";
import {RootStateType} from "../../../store/reducer";
import {driverDetailState} from "../selectors";

const driver = (state: RootStateType) => driverDetailState(state).driver

export const driverDetailSelector = createSelector(driver, (driver) => {
    return driver
})
