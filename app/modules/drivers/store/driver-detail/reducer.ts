import {Dispatch} from "redux";
import {RootStateType} from "../../../store/reducer";
import {DriverType} from "../../domain/interfaces/driverType";
import {
    DriverDetailActionsType,
    SET_DRIVER_DETAIL, setDriverDetail,
} from "./actions";
import {driversAPI} from "../../domain/API/driversAPI";

export const driverDetailInitialState = {
    driver: {} as DriverType,
}

type DriverDetailInitialStateType = typeof driverDetailInitialState

export const driverDetailReducer = (
    state = driverDetailInitialState, action: DriverDetailActionsType
): DriverDetailInitialStateType => {
    switch (action.type) {
        case SET_DRIVER_DETAIL:
            return {
                ...state,
                driver: action.driver
            }
        default:
            return state
    }
};

export const requestDriverDetail = (id: string) => {
    return async (dispatch: Dispatch<DriverDetailActionsType>, getState: RootStateType) => {
        let data = await driversAPI.getDriver(id)
        dispatch(setDriverDetail(data))
    }
}
