import {DriverType} from "../../domain/interfaces/driverType";

export const SET_DRIVER_DETAIL = 'SET_DRIVER_DETAIL';

export type setDriverDetailActionType = {
    type: typeof SET_DRIVER_DETAIL
    driver: DriverType
}

export const setDriverDetail = (driver: DriverType): setDriverDetailActionType => ({type: SET_DRIVER_DETAIL, driver});

export type DriverDetailActionsType = setDriverDetailActionType
