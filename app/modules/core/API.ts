const axios = require('axios').default;

const instance = axios.create({
    baseURL: "http://ergast.com/api/f1/",
    // headers: {'Accept': 'application/json'}
})

export default instance;
