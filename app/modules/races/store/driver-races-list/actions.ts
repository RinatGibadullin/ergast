import {RaceType} from "../../domain/interfaces/racesTypes";

export const SET_DRIVER_RACES = 'SET_DRIVER_RACES';
export const SET_DRIVER_RACES_PAGINATION = 'SET_DRIVER_RACES_PAGINATION';


export type DriverRacesPagePagination = {
    offset: number,
    limit: number
}

export type setDriverRacesActionType = {
    type: typeof SET_DRIVER_RACES
    races: Array<RaceType>
}

export type setDriverRacesPagePaginationActionType = {
    type: typeof SET_DRIVER_RACES_PAGINATION
    pagination: DriverRacesPagePagination
}

export const setDriverRaces = (races: Array<RaceType>): setDriverRacesActionType => ({type: SET_DRIVER_RACES, races});

export const setDriverRacesPagePagination = (pagination: DriverRacesPagePagination): setDriverRacesPagePaginationActionType => (
    {type: SET_DRIVER_RACES_PAGINATION, pagination}
);

export type DriverRacesListActionsType = setDriverRacesActionType | setDriverRacesPagePaginationActionType
