import {createSelector} from "reselect";
import {RootStateType} from "../../../store/reducer";
import {driverRacesState} from "../selectors";

const races = (state: RootStateType) => driverRacesState(state).races
const pagePagination = (state: RootStateType) => driverRacesState(state).pagination

export const racesSelector = createSelector(races, (races) => {
    return races
})

export const pagePaginationSelector = createSelector(pagePagination, (pagination) => {
    return pagination
})
