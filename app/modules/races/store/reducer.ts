import {combineReducers} from "redux";
import {driverRacesReducer} from "./driver-races-list/reducer";

export const racesRootReducer = combineReducers({
    driverRacesReducer
})

export type DriversListRootStateType = ReturnType<typeof racesRootReducer>
