import {AxiosResponse} from "axios";
import instance from "../../../core/API";
import {RaceType} from "../interfaces/racesTypes";

type GetDriverRacesResponseType = {
    MRData: {
        xmlns: string,
        series: string,
        limit: string,
        offset: string,
        total: string,
        RaceTable: {
            season: string,
            Races: RaceType[]
        }
    }
}

export const racesAPI = {
    getDriverRaces(id: string, offset: number, limit: number) {
        return instance.get(`drivers/${id}/races.json?limit=${limit}&offset=${offset}`)
            .then((response: AxiosResponse<GetDriverRacesResponseType>) => response.data.MRData.RaceTable.Races)
    },
}
