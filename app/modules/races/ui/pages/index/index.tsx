import React, {useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {RootStateType} from "../../../../store/reducer";
import {connect, ConnectedProps} from "react-redux";
import {pagePaginationSelector, racesSelector} from "../../../store/driver-races-list/selectors";
import {SET_DRIVER_RACES_PAGINATION} from "../../../store/driver-races-list/actions";
import {requestDriverRaces} from "../../../store/driver-races-list/reducer";
import {StackScreenProps} from "@react-navigation/stack";
import {RootStackParamList} from "../../../../../../App";
import {DriverRacesList} from "../../components/driver-races-list";

const mapState = (state: RootStateType) => ({
    races: racesSelector(state),
    pagination: pagePaginationSelector(state)
});

const mapDispatch = {
    requestDriverRaces,
    setPagePagination: () => ({type: SET_DRIVER_RACES_PAGINATION}),
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>
type DriverDetailOwnProps = StackScreenProps<RootStackParamList, 'DRIVER_RACES'>;
type DriverRacesProps = PropsFromRedux & DriverDetailOwnProps


const DriverRaces: React.FC<DriverRacesProps> = (
    {route, pagination, navigation, requestDriverRaces, races, setPagePagination, ...props}
) => {

    useEffect(() => {
        requestDriverRaces(route.params.driver_id, pagination.offset, pagination.limit)
    }, [pagination.offset])

    return <View style={styles.container}>
        <Text style={styles.title}>DRIVER RACES</Text>
        <DriverRacesList races={races}/>
        <TouchableOpacity
            style={styles.button}
            onPress={setPagePagination}
        >
            <Text>Show more</Text>
        </TouchableOpacity>
    </View>;
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    title: {
        fontSize: 50,
        justifyContent: "center"
    },
    button: {
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10
    }
});

export default connector(DriverRaces)
